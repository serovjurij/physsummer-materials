#!/usr/bin/python2
# -*- coding: UTF-8 -*-

from random import normalvariate

R1 = 1000.
R2 = 2000.
R3 = 4000.
R4 = 500.
R5m = 5000.

def fuzzy(r):
    return int(r * (1 + normalvariate(0, 0.001)))

def R_open(R5):
    r = 1/(1/(R2+R3) + 1/(R1+R5))
    return fuzzy(r)

def R_closed(R5):
    den = (R5/R1+1+R5/R4)*(1/R2+1/R3+1/R4)-R5/R4**2
    phi1 = ((1/R2+1/R3+1/R4)+(R5/R3*1/R4)) / den
    phi2 = ((1/R3)*(R5/R1+1+R5/R4)+(1/R4)) / den
    r = 1/(phi1/R1 + phi2/R2)
    return fuzzy(r)

while True:
    try:
        h = float(raw_input("Положение реостата (0-100): "))
    except ValueError:
        print "Например: 33.3"
        continue
    if h < 0 or h > 100:
        print "Число должно быть от 0 до 100"
        continue
    try:
        t = int(raw_input("Положение переключателя (0 или 1): "))
    except ValueError:
        print "Только 0 или 1"
        continue
    R5 = R5m * (h/100)**2
    if t == 0:
        r = R_open(R5)
    elif t == 1:
        r = R_closed(R5)
    else:
        print "Только 0 или 1"
        continue
    print "Сопротивление ящика", r, "Ом"
